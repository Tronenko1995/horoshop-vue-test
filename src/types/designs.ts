export interface IDesignsState {
  activeId: number | null
  designsList: IDesignsItem[]
}
export interface IDesignsItem {
  id: number
  name: string
  isPublished: boolean
  images: IDesignsItemImage[]
}
export interface IDesignsItemImage {
  id: string
  src: string
}
