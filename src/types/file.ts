export interface IFile {
  id: string
  src: string
  invalidExtension?: boolean
}
