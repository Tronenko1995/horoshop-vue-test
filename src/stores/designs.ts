import { defineStore } from 'pinia'
import type { IDesignsItem, IDesignsState } from '@/types/designs'
import { v4 as uuidv4 } from 'uuid'

export const useDesignsStore = defineStore('designs', {
  state: (): IDesignsState => ({
    activeId: null,
    designsList: [
      {
        id: 100,
        name: 'Ostrov',
        isPublished: true,
        images: [
          {
            id: uuidv4(),
            src: '/images/designs/ostrov.png'
          }
        ]
      },
      {
        id: 101,
        name: 'Flora',
        isPublished: false,
        images: [
          {
            id: uuidv4(),
            src: '/images/designs/flora.png'
          }
        ]
      },
      {
        id: 102,
        name: 'SunWear',
        isPublished: true,
        images: [
          {
            id: uuidv4(),
            src: '/images/designs/sun-wear.png'
          }
        ]
      },
      {
        id: 103,
        name: 'Flora',
        isPublished: true,
        images: [
          {
            id: uuidv4(),
            src: '/images/designs/flora.png'
          }
        ]
      },
      {
        id: 104,
        name: 'Flora',
        isPublished: false,
        images: [
          {
            id: uuidv4(),
            src: '/images/designs/flora.png'
          }
        ]
      },
      {
        id: 105,
        name: 'SunWear',
        isPublished: false,
        images: [
          {
            id: uuidv4(),
            src: '/images/designs/sun-wear.png'
          }
        ]
      },
      {
        id: 106,
        name: 'Ostrov',
        isPublished: false,
        images: [
          {
            id: uuidv4(),
            src: '/images/designs/ostrov.png'
          }
        ]
      },
      {
        id: 107,
        name: 'SunWear',
        isPublished: true,
        images: [
          {
            id: uuidv4(),
            src: '/images/designs/sun-wear.png'
          }
        ]
      }
    ]
  }),

  actions: {
    addDesign(data: IDesignsItem) {
      this.designsList.push(data)
    },
    editDesign(data: IDesignsItem) {
      const index = this.designsList.findIndex((item) => this.activeId === item.id)
      if (index + 1) {
        this.designsList[index] = { ...data }
      }
    },
    delDesign() {
      const index = this.designsList.findIndex((item) => this.activeId === item.id)
      if (index + 1) {
        this.designsList.splice(index, 1)
      }
    }
  },
  getters: {
    getSortedDesigns(): IDesignsItem[] {
      const list = [...this.designsList]
      return list.sort((a, b) => a.id - b.id)
    }
  }
})
